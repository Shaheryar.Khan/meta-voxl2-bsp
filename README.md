# meta-voxl2-bsp

## conf

### machine

```
m0052 - RB5 Flight based SOM carrier
m0054 - VOXL2
```

## recipes-android

### adb

This recipe changes default shell to bash (away from sh).

## recipes-kernel

### edk2

Patches a flag used in fastboot to prevent checking for battery voltage (EnableBatteryVoltageCheck)

### linux-msm

#### configs

Kernel defconfig files for machines above.

The `*-_defconfig` is the debug kernel build (slow boot, serial console UART enabled)

The `*-perf_defconfig` is the perf kernel build

#### dts

#### fragments

#### patches
